﻿using KnowYourMentor.Attribute;
using KnowYourMentor.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KnowYourMentor.Model.MentorViewModels
{
    public class MentorViewModel
    {
        public Mentor Mentor { get; set; }        
        public string Heading { get; set; }


        [Required(ErrorMessage = "Please Upload File")]
        [Display(Name = "Upload File")]
        [ValidateFileAttribute]
        public IFormFile file { get; set; }

    }
}
