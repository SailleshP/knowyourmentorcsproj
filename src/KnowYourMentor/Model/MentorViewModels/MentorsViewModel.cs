﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KnowYourMentor.Model.MentorViewModels
{
    public class ShowMentorViewModel
    {
        private string _showFacebookLink = "none";
        private string _showTwitterLink = "none";
        private string _showLinkedInLink = "none";
        private string _showGooglePlusLink = "none";
        private string _showQuote = "none";

        public Mentor Mentor { get; set; }

        public string ShowQuote
        {
            get
            {
                return _showQuote;
            }
            set
            {
                _showQuote = value;
            }
        }

        public string ShowTwitterLink { get
            {
                return _showTwitterLink;
            }
            set
            {
                _showTwitterLink = value;
            }
        }

        public string ShowFacebookLink
        {
            get
            {
                return _showFacebookLink;
            }
            set
            {

                _showFacebookLink = value;
            }
        }


        public string ShowGooglePlusLink { get
            {

                return _showGooglePlusLink;
            }

            set
            {
                 _showGooglePlusLink = value;

            }
        }
        public string ShowLinkedInLink { get
            {
                return _showLinkedInLink;

            }
            set
            {

                _showLinkedInLink = value;
            }
        }


        public ShowMentorViewModel setSocialSiteVisibility(ShowMentorViewModel viewModel)
        {

            if(this.Mentor.FacebookLink!=null)
            {
                this.ShowFacebookLink = "inline-block";
            }

            if (this.Mentor.TwitterLink != null)
            {
                this.ShowTwitterLink = "inline-block";
            }
            if (this.Mentor.GooglePlus != null)
            {
                this.ShowGooglePlusLink = "inline-block";
            }
            if (this.Mentor.LinkedIn != null)
            {
                this.ShowLinkedInLink = "inline-block";
            }
            if(this.Mentor.FavouriteQuote!=null)
            {
                this.ShowQuote = "inline-block";

            }

            return viewModel;

        }

    }
}
