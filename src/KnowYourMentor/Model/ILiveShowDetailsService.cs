﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KnowYourMentor.Model
{
    public interface ILiveShowDetailsService
    {
        Task<Episodes> LoadAsync();

       // Task SaveAsync(LiveShowDetails liveShowDetails);
    }
}
