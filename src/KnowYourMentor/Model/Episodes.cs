﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KnowYourMentor.Model
{
    public class Episodes
    {
        public IList<MentorEpisode> Shows { get; set; }

        public string MoreShowsUrl { get; set; }
    }
}
