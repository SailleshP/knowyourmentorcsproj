﻿using System.ComponentModel.DataAnnotations;

namespace KnowYourMentor.ViewModel.AccountViewModels
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
