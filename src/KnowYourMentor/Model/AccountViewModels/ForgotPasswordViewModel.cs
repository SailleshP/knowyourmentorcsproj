﻿using System.ComponentModel.DataAnnotations;

namespace KnowYourMentor.ViewModel.AccountViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
