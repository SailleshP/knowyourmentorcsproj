﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KnowYourMentor.Model
{
    public class AppSettings
    {
    public  string YouTubeApiKey { get;  set; }
    public string YouTubePlaylistId { get;  set; }
    public string YouTubeApplicationName { get; set; }
        
    }
}
