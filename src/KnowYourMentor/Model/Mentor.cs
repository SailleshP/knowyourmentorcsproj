﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KnowYourMentor.Model
{
    public class Mentor
    {


        [BsonId]
        public ObjectId ID { get; set; }
        
        [BsonElement]
        [Required]
        public string FirstName { get; set; }


        [BsonElement]
        [Required]
        public string LastName { get; set; }

        [Required]
        [BsonElement]
        [StringLength(180,ErrorMessage ="Summary should be less than 180 characters")]
        public string Summary { get; set; }


        [Required]
        [BsonElement]
        public string Description { get; set; }


        [BsonElement]
        public string MentorImage { get; set; }

        [BsonElement]
        [Display(Name = "Favourite Quotes")]
        public string FavouriteQuote { get; set; }

        [BsonElement]
        [Display(Name = "Twitter Profile")]
        public string TwitterLink { get; set; }


        [BsonElement]
        [Display(Name = "Facebook Profile")]
        public string FacebookLink { get; set; }
        [BsonElement]

        [Display(Name = "Google+ Profile")]
        public string GooglePlus { get; set; }
        [BsonElement]

        [Display(Name = "Linked In Profile")]
        public string LinkedIn { get; set; }
    }
}
