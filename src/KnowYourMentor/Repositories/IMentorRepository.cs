﻿using KnowYourMentor.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KnowYourMentor.Repositories
{
    public interface IMentorRepository
    {

        bool Add(Mentor mentor);
        IEnumerable<Mentor> GetMentors();
        IEnumerable<Mentor> GetMentorById(int id);


    }
}
