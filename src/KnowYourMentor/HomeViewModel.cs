﻿namespace KnowYourMentor.Controllers
{
    internal class HomeViewModel
    {
        public object AdminMessage { get; set; }
        public object LiveShowEmbedUrl { get; set; }
        public object LiveShowHtml { get; set; }
        public object MoreShowsUrl { get; set; }
        public object NextShowDateUtc { get; set; }
        public object PreviousShows { get; set; }
    }
}