﻿using KnowYourMentor.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KnowYourMentor.Model;

namespace KnowYourMentor.Persistence.Repositories
{
    public class MentorRepository : IMentorRepository
    {

        private readonly MongoDBContext _context;

        public MentorRepository(MongoDBContext context)
        {
            _context = context;
        }


        public bool Add(Mentor mentor)
        {
            bool result = _context.add(mentor);
            return result;
        }

        public IEnumerable<Mentor> GetMentorById(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Mentor> GetMentors()
        {
            throw new NotImplementedException();
        }
    }
}
