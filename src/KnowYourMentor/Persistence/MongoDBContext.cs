﻿using KnowYourMentor.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KnowYourMentor.Persistence
{


    //private readonly AppSettings _appSettings;
    //private readonly IMemoryCache _cache;
    ////private readonly TelemetryClient _telemetry;

    //public YouTubeShowsService(

    //    IOptions<AppSettings> appSettings,
    //    IMemoryCache memoryCache
    //    /*TelemetryClient telemetr*/)
    //{

    //    _appSettings = appSettings.Value;
    //    _cache = memoryCache;
    //    //_telemetry = telemetry;
    //}


    public class MongoDBContext
    {

        MongoClient _client;
        MongoServer _server;
        public MongoDatabase _database;
        public MongoDBContext( IOptions<MongoDbSettings> options)        //constructor   
        {

            var MongoDatabaseName = options.Value.Database;
            //var MongoUsername = ConfigurationManager.AppSettings["MongoUsername"]; //demouser  
            //var MongoPassword = ConfigurationManager.AppSettings["MongoPassword"]; //Pass@123  
            var MongoPort = 27017;
            var MongoHost = "localhost";

            // Creating MongoClientSettings  
            var settings = new MongoClientSettings
            {
                //Credentials = new[] { credential },
                Server = new MongoServerAddress(MongoHost, Convert.ToInt32(MongoPort))
            };
            _client = new MongoClient(settings);
            _server = _client.GetServer();
            _database = _server.GetDatabase(MongoDatabaseName);

        }


        public bool Update(Mentor mentor, ObjectId Id)
        {

            mentor.ID = Id;
            var res = Query<Mentor>.EQ(pd => pd.ID, Id);
            var operation = Update<Mentor>.Replace(mentor);
            var result = _database.GetCollection<Mentor>("Mentors").Update(res, operation);
            if (result.UpdatedExisting)
            {
                return true;
            }
            else
            {
                return false;

            }

        }


        public bool add(Mentor mentor)
        {
            var document = this._database.GetCollection<BsonDocument>("Mentors");
            var query = Query.And(Query.EQ("FirstName", mentor.FirstName),
                        Query.EQ("LastName", mentor.LastName));

            // Will Return Count if same document exists else will Return 0 
            var count = document.FindAs<Mentor>(query).Count();

            //if it is 0 then only we are going to insert document
            if (count == 0)
            {
                var result = document.Insert(mentor);
            return true;

            }
            else
            {
                return false;
            }

        }


        public IEnumerable<Mentor> getAllMentors()
        {
            return this._database.GetCollection<Mentor>("Mentors").FindAll().ToList();

        }



        public Mentor getMentorDetails(string Id)
        {
            var mentorObjectid = Query<Mentor>.EQ(p => p.ID, new ObjectId(Id));
            return this._database.GetCollection<Mentor>("Mentors").FindOne(mentorObjectid);

        }

    }
}
