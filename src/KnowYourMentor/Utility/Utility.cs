﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KnowYourMentor.Utility
{
    public static class Utility
    {
        public static string Tobase64FileRepresentation( IFormFile file)
        {
            byte[] buffer = null;
            using (var stream = file.OpenReadStream())
            {
                buffer = new byte[stream.Length];
                stream.Read(buffer, 0, (int)stream.Length);
            }

            return Convert.ToBase64String(buffer); 
        }



    }
}
