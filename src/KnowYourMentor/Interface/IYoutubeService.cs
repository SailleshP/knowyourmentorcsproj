﻿using KnowYourMentor.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KnowYourMentor.Interface
{
    public interface IYoutubeService
    {
        Task<Episodes> GetShowsList();
       
    }
}
