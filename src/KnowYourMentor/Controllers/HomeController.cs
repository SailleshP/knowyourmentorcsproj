﻿using System;
using Microsoft.AspNetCore.Mvc;
using KnowYourMentor.Model;
using KnowYourMentor.Interface;

namespace KnowYourMentor.Controllers
{
    public class HomeController : Controller
    {
       

        private readonly IYoutubeService _iYoutubeService;

        public HomeController(IYoutubeService iYoutubeService)
        {
            _iYoutubeService = iYoutubeService;
          
        }




        public IActionResult Shows()
        {
            var mentorEpisode = new MentorEpisode()
            {
                Description="Sample",
                Provider="youtube provider",
                ShowDate=DateTime.Now,
                ThumbnailUrl= "https://www.google.co.in/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&ved=0ahUKEwiLpJXhivfRAhVJtI8KHQBuADMQjxwIAw&url=https%3A%2F%2Fwww.drupal.org%2Fsandbox%2Fmaskedjellybean%2F2312143&psig=AFQjCNGltue8AchGvPVQmALWWBvRE49GCg&ust=1486319767904830",
                Title="Know your mentor.com",
                

            };



            return View(mentorEpisode);

        }


        public ActionResult Contact()
        {
            return View();

        }




        public ActionResult About()
        {

            return View();


        }

       public ActionResult AllShows()
        {
            var playlist = _iYoutubeService.GetShowsList().Result;
            return View("Shows", playlist);

        }

       
    }
}
