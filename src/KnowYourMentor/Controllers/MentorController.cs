﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using KnowYourMentor.Model.MentorViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Net.Http.Headers;
using System.IO;
using Microsoft.AspNetCore.Authorization;
using KnowYourMentor.Model;
using AspNetCore.Identity.MongoDB;
using KnowYourMentor.Persistence;
using MongoDB.Driver.Builders;
using MongoDB.Bson;
using KnowYourMentor.Repositories;
using KnowYourMentor.Utility;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace KnowYourMentor.Controllers
{
    public class MentorController : Controller
    {


        private IHostingEnvironment _env;
        private readonly MongoDBContext _context;
        private readonly IMentorRepository _iMentorRepository;
        public MentorController(MongoDBContext context, IHostingEnvironment env,IMentorRepository iMentorRepository)
        {
            _context = context;
            _env = env;
            _iMentorRepository = iMentorRepository;

        }

       
        
      


        // GET: /<controller>/
        [AllowAnonymous]
        public IActionResult Index()
        {
            var mentors = _context.getAllMentors();

            foreach (var item in mentors)
            {
                item.MentorImage= string.Format("data:image/png;base64,{0}", item.MentorImage);
            }
            

            return View(mentors);
        }


       

        [Authorize]
        public IActionResult Update(MentorViewModel viewModel,string Id)
        {

            if (!ModelState.IsValid)
            {
                viewModel.Mentor.ID =new ObjectId(Id);
                return View("Edit", viewModel);
            }
            if (viewModel.file.Length > 0)
            {
                string base64FileRepresentation = KnowYourMentor.Utility.Utility.Tobase64FileRepresentation(viewModel.file);
                viewModel.Mentor.MentorImage = base64FileRepresentation;
            }
            else
            {

                ModelState.AddModelError("Image", "No Image found");

            }
            var status=  _context.Update(viewModel.Mentor, new ObjectId(Id));
            if(status)
            { 
            return RedirectToAction("Index");
            }
            else
            {
                return View("Edit", viewModel);
            }

        }

        [Authorize]
        [HttpGet]
        public IActionResult Create()
        {
            var mentorViewModel = new MentorViewModel()
            {
                Heading = "Add a Mentor"
            
            };

            return View(mentorViewModel);
        }

        [Authorize]
        [HttpPost]
        public IActionResult create(MentorViewModel viewModel,IFormFile file)
        {

            if (!ModelState.IsValid)
            { 
                viewModel.Heading = "Add a Mentor";
                return View(viewModel);
            }
                    if (file.Length > 0)
                    {
                        string base64FileRepresentation = KnowYourMentor.Utility.Utility.Tobase64FileRepresentation(file);
                        viewModel.Mentor.MentorImage = base64FileRepresentation;
                    }
            _context.add(viewModel.Mentor);
            return RedirectToAction("Index", "Mentor");
        }

        [AllowAnonymous]
        public IActionResult GetMentorById(string Id)
        {
            var showMentorViewModel = new ShowMentorViewModel()
            {
                Mentor= _context.getMentorDetails(Id)
            };
            showMentorViewModel.Mentor.MentorImage = string.Format("data:image/png;base64,{0}", showMentorViewModel.Mentor.MentorImage);
            showMentorViewModel = showMentorViewModel.setSocialSiteVisibility(showMentorViewModel);
            return View("Details", showMentorViewModel);
        }


        [Authorize]
        public IActionResult Edit(string Id)
        {
            var mentorViewModel = new MentorViewModel()
            {
                Mentor = _context.getMentorDetails(Id)
            };
            mentorViewModel.Mentor.MentorImage = string.Format("data:image/png;base64,{0}", mentorViewModel.Mentor.MentorImage);
            return View(mentorViewModel);
        }


    }
}
