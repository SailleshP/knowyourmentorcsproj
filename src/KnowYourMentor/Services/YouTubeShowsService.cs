﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.ApplicationInsights;
using System.Security.Claims;
using KnowYourMentor.Model;
using Google.Apis.YouTube.v3.Data;
using System.Globalization;
using Google.Apis.Services;
using Google.Apis.YouTube.v3;
using System.Text.Encodings.Web;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using KnowYourMentor.Interface;

namespace KnowYourMentor.Services
{
    public class YouTubeShowsService: IYoutubeService
    {

        private readonly AppSettings _appSettings;
        private readonly IMemoryCache _cache;
        //private readonly TelemetryClient _telemetry;

        public YouTubeShowsService(
           
            IOptions<AppSettings> appSettings,
            IMemoryCache memoryCache
            /*TelemetryClient telemetr*/)
        {
           
            _appSettings = appSettings.Value;
            _cache = memoryCache;
            //_telemetry = telemetry;
        }



        

       

        public async Task<Episodes> GetShowsList()
        {
            using (var client = new YouTubeService(new BaseClientService.Initializer
            {
                ApplicationName = Convert.ToString(_appSettings.YouTubeApplicationName),
                ApiKey = _appSettings.YouTubeApiKey
            }))
            {
                var listRequest = client.PlaylistItems.List("snippet");
                listRequest.PlaylistId = _appSettings.YouTubePlaylistId;
                listRequest.MaxResults = 5 * 3; // 5 rows of 3 episodes

                PlaylistItemListResponse playlistItems = null;
                var started = Timing.GetTimestamp();
                    playlistItems = await listRequest.ExecuteAsync();

                var result = new Episodes();

                result.Shows = playlistItems.Items.Select(item => new MentorEpisode
                {
                    Provider = "YouTube",
                    ProviderId = item.Snippet.ResourceId.VideoId,
                    Title = item.Snippet.Title,
                    Description = item.Snippet.Description,
                    ShowDate = DateTimeOffset.Parse(item.Snippet.PublishedAtRaw, null, DateTimeStyles.RoundtripKind),
                    ThumbnailUrl = item.Snippet.Thumbnails.High.Url,
                    Url = GetVideoUrl(item.Snippet.ResourceId.VideoId, item.Snippet.PlaylistId, item.Snippet.Position ?? 0)
                }).ToList();

                if (!string.IsNullOrEmpty(playlistItems.NextPageToken))
                {
                    result.MoreShowsUrl = GetPlaylistUrl(_appSettings.YouTubePlaylistId);
                }

                return result;
            }
        }


        private static string GetUsefulBitsFromTitle(string title)
        {
            if (title.Count(c => c == '-') < 2)
            {
                return string.Empty;
            }

            var lastHyphen = title.LastIndexOf('-');
            if (lastHyphen >= 0)
            {
                var result = title.Substring(lastHyphen + 1);
                return result;
            }

            return string.Empty;
        }

        private static string GetVideoUrl(string id, string playlistId, long itemIndex)
        {
            var encodedId = UrlEncoder.Default.Encode(id);
            var encodedPlaylistId = UrlEncoder.Default.Encode(playlistId);
            var encodedItemIndex = UrlEncoder.Default.Encode(itemIndex.ToString());

            return $"https://www.youtube.com/watch?v={encodedId}&list={encodedPlaylistId}&index={encodedItemIndex}";
        }

        private static string GetPlaylistUrl(string playlistId)
        {
            var encodedPlaylistId = UrlEncoder.Default.Encode(playlistId);

            return $"https://www.youtube.com/playlist?list={encodedPlaylistId}";
        }




    }
}
